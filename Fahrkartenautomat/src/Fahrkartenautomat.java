﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static void ladebalken() {
		for (int i = 0; i < 32; i++)
	       {
	          System.out.print(".");
	       
	          warte(50);
	       }
	       System.out.println("\n\n");
	}
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein(e) werden ausgegeben");
	       for (int i = 0; i < 16; i++)
	       {
	          System.out.print("=");
	      
	          warte(200);
	       }
	       System.out.println("\n\n");
	}
	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag){
		double eingezahlterGesamtbetrag = 0.0;
		double d = 0.0;
		double eingeworfeneMünze = 0.0;
		double rückgabebetrag;
		Scanner tastatur2 = new Scanner(System.in);
		
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   d= zuZahlenderBetrag - eingezahlterGesamtbetrag;
	    	   System.out.printf("Noch zu zahlen: %.2f€ \n" ,d);
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro (geht nur als double mit Komma)): ");
	    	   eingeworfeneMünze =  tastatur2.nextDouble();
	    	   if(eingeworfeneMünze > 2) {
	    		   System.out.println("Maximal 2 Euro einwerfen!, Sie bekommen die "+ eingeworfeneMünze+" Euro zurück");
	    		   eingeworfeneMünze = 0;
	    	   }
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	       //tastatur2.close();
	       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       return rückgabebetrag;
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println("*klirr*");
		warte(700);
		System.out.println(betrag + " " +einheit);
		
		warte(700);
	}
	
	public static void rueckgeldAusgeben(double rückgabebetrag) {
		
		if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n" ,  rückgabebetrag );
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag > 1.99) // 2 EURO-Münzen  WEGEN RUNDUNGSFEHLERN
	           {
	        	  muenzeAusgeben(2, "EURO");
	        	  
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 0.99) // 1 EURO-Münzen
	           {
	        	   muenzeAusgeben(1, "EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.49) // 50 CENT-Münzen
	           {
	        	   muenzeAusgeben(50, "CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag > 0.19) // 20 CENT- Münzen
	           {
	        	   muenzeAusgeben(20, "CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag > 0.09) // 10 CENT-Münzen
	           {
	        	   muenzeAusgeben(10, "CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
	           {
	        	   muenzeAusgeben(5, "CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	}
	
	public static double fahrkartenbestellungErfassen() {
		double zuZahlenderBetrag = 0.0;
		int anzahlTickets ; 
		double ticketpreis =0.00;
		int auswahl;
		boolean eingabeFehlerhaft = false ;
		boolean weiterkaufen = true;
		Scanner tastatur = new Scanner(System.in);
		double[] preise = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		String[] bezeichnungen = {"Einzelfahrschein Berlin AB ",
				"Einzelfahrschein Berlin BC ", 
				"Einzelfahrschein Berlin ABC ", 
				"Kurzstrecke ",
				"Tageskarte Berlin AB ",
				"Tageskarte Berlin BC ", 
				"Tageskarte Berlin ABC ",
				"Kleingruppen-Tageskarte Berlin AB ",
				"Kleingruppen-Tageskarte Berlin BC" ,
				"Kleingruppen-Tageskarte Berlin ABC "};
		
		System.out.println("Willkommen im Fahrkartenautomat!");
		warte(1500);
		
		
		while(weiterkaufen) {
			System.out.println("Wählen Sie eine Option aus:\n");
			for(int i=0;i<bezeichnungen.length;i++) {
				System.out.println(bezeichnungen[i]+" ["+preise[i]+" EUR]" + " ("+(i+1)+")");
			}
			System.out.println("Bezahlen"+ " ("+(bezeichnungen.length+1)+")");
			System.out.print("Ihre Auswahl:");
			auswahl = tastatur.nextInt();
		
			if(auswahl>0 && auswahl<=bezeichnungen.length) {
				do {
					System.out.print("Anzahl der Tickets (min 1 max 10): ");
					anzahlTickets= tastatur.nextInt();
					if(anzahlTickets > 10 || anzahlTickets <1) {
						System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
					}
				}while(anzahlTickets > 10 || anzahlTickets <1);
				ticketpreis = preise[(auswahl-1)];
				zuZahlenderBetrag += anzahlTickets*ticketpreis; 
				System.out.println("");
				System.out.println("Zwischensumme = "+zuZahlenderBetrag);
				System.out.println("");
				ladebalken();
			}else if(auswahl == (bezeichnungen.length +1)) {
				weiterkaufen = false;
				if (ticketpreis==0) {System.out.println("Sie haben kein Ticket ausgewählt und bekommen ein Blanko-Ticket..");}
			}else {
				System.out.println(">> falsche Eingabe << ");
			}
			
		
		}
	
	     
	       
		//tastatur.close(); // führt zu einer exception
		return zuZahlenderBetrag;
	}
	
    public static void main(String[] args)
    {
    	boolean programmWiederholenLassen = true;
    	while(programmWiederholenLassen) {
    	
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double d;
       int anzahlTickets; // int weil ganze zahl und kann mit double verrechnet werden
       
       
   	 
   zuZahlenderBetrag= fahrkartenbestellungErfassen();
   rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
   fahrkartenAusgeben();
   rueckgeldAusgeben(rückgabebetrag);
   


    
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       warte(2000);
       System.out.println("");
       System.out.println("");
       for (int i = 0; i < 20; i++)
       {
          System.out.print(".");
       
          warte(200);
       }
       System.out.println("\n\n");
       
    	}// Ende der Wiederholungsschleife
    }//Ende der Main-Methode
}// Ende der Klasse