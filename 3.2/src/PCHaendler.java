
import java.util.Scanner;

public class PCHaendler {


	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		System.out.println("was m�chten Sie bestellen?");
		String artikel = liesString(myScanner);
		
		System.out.println("frage 2?");

		
		
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt(myScanner);

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble(myScanner);

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble(myScanner);

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

	
	public static String liesString(Scanner myScanner) {
	//	Scanner myScanner = new Scanner(System.in);
		String artikel = myScanner.next();
		return artikel;
	}
	public static int liesInt(Scanner myScanner) {
		//Scanner myScanner2 = new Scanner(System.in);
		int artikel = myScanner.nextInt();
		return artikel;
	}
	
	public static double liesDouble(Scanner myScanner) {
	
		double wert = myScanner.nextDouble();
		return wert;
	}
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double wert = anzahl * nettopreis;
		return wert;
	}
	public static double berechneGesamtbruttopreis(int mwst, double nettogesamtpreis) {
		double faktor = 1.0+(mwst / 100);
		double wert = nettogesamtpreis * faktor;
		return wert;
		
	}
	


}